using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace BlazorMuGames2
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }
        public static List<string> IpUrls()
        {
            var url = new List<string>();
            IPAddress[] ipv4Addresses = Array.FindAll(Dns.GetHostEntry(Dns.GetHostName()).AddressList, a => a.AddressFamily == AddressFamily.InterNetwork);
            url.Add("http://localhost:5000");
            for (int i = 0; i != ipv4Addresses.Length; i++)
            {
                url.Add("http://" + ipv4Addresses[i].ToString() + ":5000");
            }
            return url;
        }
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                    webBuilder.UseUrls(IpUrls().ToArray());
                });
    }
}
