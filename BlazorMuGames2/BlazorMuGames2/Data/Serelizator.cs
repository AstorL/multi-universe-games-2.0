﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.InteropServices;
using Newtonsoft.Json;

namespace BlazorMuGames2.Data
{
    unsafe public struct memConf
    {
        public IntPtr memIntPtr;
        public int length;
        public byte* memBytePtr;
    }
    public class Serelizator
    {
        public Serelizator() { }
        public override string ToString()=>base.ToString();
        public string ToStringByteArray(byte[] buf)
        {
            string str = null;
            var cow = 0;
            while (cow < buf.Length)
            {
                str += buf[cow++].ToString();
            }
            return str;
        }

        public byte[] ObjectToByteArray(object obj)
        {
            var wbin = new BinaryFormatter();
            var wmem = new MemoryStream();
            wbin.Serialize(wmem, obj);
            wmem.Close();
            return wmem.ToArray();
        }
     
        public object ByteArrayToObject(byte[] rmem)
        {
            var rbin = new BinaryFormatter();
            var cow = 0;
            var rStream = new MemoryStream();
            while (cow < rmem.Length)
            {
                rStream.WriteByte(rmem[cow++]);
            }
                rStream.Position = 0;
                object obj = (object)rbin.Deserialize(rStream);
            rStream.Close();
            return obj;
        }
        unsafe public memConf ByteArrayToMemory(byte[] mem)
        {
            memConf conf;

            conf.length = mem.Length;
            conf.memIntPtr = Marshal.AllocHGlobal(mem.Length);
            conf.memBytePtr = (byte*)conf.memIntPtr.ToPointer();

            using (UnmanagedMemoryStream writeStream = new UnmanagedMemoryStream(conf.memBytePtr, mem.Length, mem.Length, FileAccess.Write))
            {
                writeStream.Write(mem, 0, mem.Length);
                writeStream.Close();
            }
            return conf;
        }
        unsafe public byte[] MemoryToByteArray(memConf conf)
        {
            var rmem = new byte[conf.length];
            using (UnmanagedMemoryStream readStream = new UnmanagedMemoryStream(conf.memBytePtr, conf.length, conf.length, FileAccess.Read))
            {
                readStream.Read(rmem, 0, conf.length);
                readStream.Close();
            }
            Marshal.FreeHGlobal(conf.memIntPtr);
            return rmem;
        }
        
        public string ByteArrayToJSON(byte[] rmem) => JsonConvert.SerializeObject(ByteArrayToObject(rmem));
        public byte[] JSONToByteArray(string json) => ObjectToByteArray(JsonConvert.DeserializeObject<object>(json));
    }
}
