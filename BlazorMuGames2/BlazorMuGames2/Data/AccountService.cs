﻿using System;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using System.Collections.ObjectModel;

namespace BlazorMuGames2.Data
{
    public class AccountService : MariaDBResolver<Account>
    {
        private IConfiguration Configuration;
        private Serelizator serelizator = new Serelizator();
        private MySqlCommand SelectAccount = new MySqlCommand("SELECT * FROM `account`");
        private MySqlCommand InsertAccount = new MySqlCommand("INSERT INTO account (data_acc) VALUES (@Bytes)");
        public AccountService(IConfiguration Configuration) : base(Configuration) => this.Configuration = Configuration;

        protected override ObservableCollection<Account> GetData(MySqlCommand comm)
        {
            var AccountService = new ObservableCollection<Account>();
            var BufferList = GetBufferedData(comm);
            try
            {
                foreach (var AccountBytes in BufferList)
                {
                    var account = (Account)serelizator.ByteArrayToObject((byte[])AccountBytes[1]);
                    account.Id = (uint)AccountBytes[0];
                    AccountService.Add(account);
                }
            }
            catch(Exception E)
            {
                Console.WriteLine($"Ошибка в получении учетных записей \n {E.Message} \n {E.StackTrace}\n");
            }
            BufferList.Clear();
            return AccountService;
        }
        public ObservableCollection<Account> GetAccountsCollection() => GetData(SelectAccount);
        public void AddAccount(Account account)
        {
            account.EnycryptPassword();
            account.RegistrationDate = DateTime.UtcNow;
            InsertAccount.Parameters.AddWithValue("@Bytes", serelizator.ObjectToByteArray(account));
            RunCommand(InsertAccount);
            InsertAccount.Parameters.Clear();
        }
    }
}
