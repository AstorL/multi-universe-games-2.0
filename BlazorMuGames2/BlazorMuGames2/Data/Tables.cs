﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Collections.ObjectModel;

namespace BlazorMuGames2.Data
{
    [Serializable]
    public class Account
    {
        public uint Id { get; set; }
        [Required(ErrorMessage="Введите имя пользователя")]
        public string Login { get; set; }
        [Required(ErrorMessage = "Введите пароль")]
        public string Password { get; set; }
        public DateTime RegistrationDate { get; set; }
        public void EnycryptPassword()
        {
            var hashbytes = SHA512.Create().ComputeHash(Encoding.UTF8.GetBytes(Login + Password));
            var hashbuilder = new StringBuilder();
            foreach (var hashbyte in hashbytes)
            {
                hashbuilder.Append(hashbyte.ToString("x2"));
            }
            Password = hashbuilder.ToString();
        }
    }

    public class NavigationLink
    {
        public int IdLink { get; set; }
        public string Adress { get; set; }
        public string Name { get; set; }
        public string Icon { get; set; }
        public NavigationLink(int IdLink, string Adress, string Name, string Icon)
        {
            this.IdLink = IdLink;
            this.Adress = Adress;
            this.Name = Name;
            this.Icon = Icon;
        }
    }
    [Serializable]
    public class Data
    {
        public uint Id { get; set; }
        public string DataName { get; set; }
        [Required(ErrorMessage ="Введите данные")]
        public string DataText { get; set; }
        public DateTime CreationDate { get; set; }

        public Data() { }
        public Data(uint Id, string DataText, DateTime CreationDate)
        {
            this.Id = Id;
            this.DataText = DataText;
            this.CreationDate = CreationDate;
        }

    }
    public class Forum
    {
        public uint Id { get; set; }
        [Required(ErrorMessage="Введите имя форума")]
        public string ForumName { get; set; }
    }
    public class Chat
    {
        public uint Id { get; set; }
        [Required(ErrorMessage = "Введите имя чата")]
        public string ChatName { get; set; }
    }

    public class Message
    {
        public uint Id { get; set; }
        public Account Account{get; set;}
        public Data Data { get; set; }
        public Message()
        {
            Account = new Account();
            Data = new Data();
        }
    }
}
