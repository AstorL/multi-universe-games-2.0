using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using System.Collections.ObjectModel;

namespace BlazorMuGames2.Data
{
    public class NavigationService : MariaDBResolver<NavigationLink>
    {
        private IConfiguration Configuration;
        private MySqlCommand NavLinksRequest = new MySqlCommand("SELECT * FROM links;");

        public NavigationService(IConfiguration Configuration) : base(Configuration) => this.Configuration = Configuration;

        protected override ObservableCollection<NavigationLink> GetData(MySqlCommand comm)
        {
            var NavigationService = new ObservableCollection<NavigationLink>();
            var Buffer = GetBufferedData(comm);
            foreach (var link in Buffer)
            {
                try
                {
                    NavigationService.Add
                    (
                        new NavigationLink
                        (
                            (int)link[0],
                            (string)link[1],
                            (string)link[2],
                            (string)link[3]
                        )
                    );
                }
                catch (Exception E)
                {
                    Console.WriteLine($"Ошибка в получении навигационных ссылок\n {E.Message}\n {E.StackTrace}\n");
                }
            }
            Buffer.Clear();
            return NavigationService;
        }

        public ObservableCollection<NavigationLink> GetNavigationLinks() => 
            GetData(NavLinksRequest);
    }
}