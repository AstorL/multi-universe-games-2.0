﻿using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using System.Linq;
using System.Collections.ObjectModel;
using System;

namespace BlazorMuGames2.Data
{
    public class ChatService : MariaDBResolver<Chat>
    {
        private IConfiguration Configuration;
        private MySqlCommand SelectChats = new MySqlCommand("SELECT * FROM chat");
        private MySqlCommand SelectChatsByForum = new MySqlCommand(@"SELECT chat.id_chat, chat.chat_name FROM (chat JOIN chattoforum)
        WHERE chat.id_chat = chattoforum.id_chat AND chattoforum.id_forum = @Forum");
        private MySqlCommand InsertChat = new MySqlCommand("INSERT INTO chat(chat_name)VALUES(@ChatName)");
        private MySqlCommand MountForum = new MySqlCommand("INSERT INTO chattoforum(id_chat, id_forum)VALUES(@Chat,@Forum)");
        public ChatService(IConfiguration Configuration) : base(Configuration) => this.Configuration = Configuration;
        protected override ObservableCollection<Chat> GetData(MySqlCommand command)
        {
            var ChatSerivice = new ObservableCollection<Chat>();
            var BufferedData =  GetBufferedData(command);
            try
            {
                foreach (var chatbuffer in BufferedData)
                {
                    ChatSerivice.Add(new Chat { Id = (uint)chatbuffer[0], ChatName = (string)chatbuffer[1] });
                }
            }
            catch(Exception E)
            {
                Console.WriteLine($"Ошибка в получении чатов\n {E.Message}\n {E.StackTrace}\n");
            }
            return ChatSerivice;
        }

        public ObservableCollection<Chat> GetChats() => GetData(SelectChats);

        public ObservableCollection<Chat> GetChatsByForum(Forum forum)
        {
            SelectChatsByForum.Parameters.AddWithValue("@Forum", forum.Id);
            var result =  GetData(SelectChatsByForum);
            SelectChatsByForum.Parameters.Clear();
            return result;
        }
        public void InsertChats(Forum forum, Chat chat)
        {
            var mountablechat = from chats in GetChats() where chats.ChatName == chat.ChatName select chats;
            if (mountablechat.Count() == 1)
            {
                MountForum.Parameters.AddWithValue("@Chat", mountablechat.First().Id);
                MountForum.Parameters.AddWithValue("@Forum", forum.Id);
                RunCommand(MountForum);
                MountForum.Parameters.Clear();
            }
            else
            {
                InsertChat.Parameters.AddWithValue("@ChatName", chat.ChatName);
                RunCommand(InsertChat);
                InsertChat.Parameters.Clear();
                var insertedchat = from chats in GetChats() where chats.ChatName == chat.ChatName select chats.Id;
                MountForum.Parameters.AddWithValue("@Chat", insertedchat.First());
                MountForum.Parameters.AddWithValue("@Forum", forum.Id);
                RunCommand(MountForum);
                MountForum.Parameters.Clear();
            }
        }
    }
    public class ForumService : MariaDBResolver<Forum>
    {
        private IConfiguration Configuration;
        private MySqlCommand SelectForum = new MySqlCommand("SELECT * FROM forum");
        private MySqlCommand InsertForum = new MySqlCommand("INSERT INTO forum(forum_name) VALUES(@Name)");
        public ForumService(IConfiguration Configuration) : base(Configuration) => this.Configuration = Configuration;
        protected override ObservableCollection<Forum> GetData(MySqlCommand command)
        {
            var ForumService = new ObservableCollection<Forum>();
            var BufferedData =  GetBufferedData(command);
            try
            {
                foreach (var forumbuffer in BufferedData)
                {
                    ForumService.Add(new Forum { Id= (uint)forumbuffer[0], ForumName = (string)forumbuffer[1] });
                }
            }
            catch(Exception E)
            {
                Console.WriteLine($"Ошибка в получении форумов\n {E.Message}\n {E.StackTrace}\n");
            }

            return ForumService;
        }
        public ObservableCollection<Forum> GetForums() =>  GetData(SelectForum); 
        public void InsertForums(Forum forum)
        {
            InsertForum.Parameters.AddWithValue("@Name", forum.ForumName);
             RunCommand(InsertForum);
            InsertForum.Parameters.Clear();
        }
    }
}
