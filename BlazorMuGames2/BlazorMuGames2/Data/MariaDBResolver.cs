﻿using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;

namespace BlazorMuGames2.Data
{
    public abstract class MariaDBResolver<T>
    {
        private MySqlConnection Conn;
        private IConfiguration Configuration;
        public ObservableCollection<string> ColumnNames { get; }
        public MariaDBResolver(IConfiguration Configuration)
        {
            this.Configuration = Configuration;
            Conn = new MySqlConnection(this.Configuration.GetSection("ExternalConnections").GetSection("SiteDatabase").Value);
            ColumnNames = new ObservableCollection<string>();
            Console.WriteLine($"Подключение к базе данных. Статус подключения {Conn.State}.");
            try
            {
                Conn.Open();
                Console.WriteLine($"Подключение к базе данных. Статус подключения {Conn.State}.");
            }
            catch (Exception E)
            {
                Conn.Close();
                Console.WriteLine($"Подключение к базе данных прервано из-за ошибки{E.Message}.\n" +
                    $" Статус подключения {Conn.State}.\n Ошибка произошла в следующем участке кода\n {E.StackTrace}");
            }
        }

        protected List<object[]> GetBufferedData(MySqlCommand command)
        {
            ColumnNames.Clear();
            try
            {
                command.Connection = Conn;
            }
            catch(Exception E)
            {
                Console.WriteLine($"Ошибка в работе считывателя{E.Message}.\n" +
                         $" Статус подключения {Conn.State}.\n Ошибка произошла в следующем участке кода\n {E.StackTrace}");
            }
            var QueryList = new List<object[]>();
            try
            {
                if (Conn != null && Conn.State == ConnectionState.Open)
                {
                    try
                    {
                        var DataReader = command.ExecuteReader();
                        while (DataReader.Read())
                        {
                            var myObject = new object[DataReader.FieldCount];
                            DataReader.GetValues(myObject);
                            for (int i = 0; i != DataReader.FieldCount; i++)
                            {
                                ColumnNames.Add(DataReader.GetName(i));
                            }
                            QueryList.Add(myObject);
                        }
                        DataReader.Close();
                    }
                    catch(Exception E)
                    {
                        Console.WriteLine($"Ошибка в работе считывателя{E.Message}.\n" +
                         $" Статус подключения {Conn.State}.\n Ошибка произошла в следующем участке кода\n {E.StackTrace}");
                    }
                }
                    
            }
            catch (Exception E)
            {
                Console.WriteLine($"Подключение к базе данных прервано из-за ошибки{E.Message}.\n" +
                    $" Статус подключения {Conn.State}.\n Ошибка произошла в следующем участке кода\n {E.StackTrace}");
            }
            return QueryList;
        }

        protected void RunCommand(MySqlCommand comm)
        {
            try
            {
                if (Conn != null && Conn.State == ConnectionState.Open)
                {
                    comm.Connection = Conn;
                     comm.ExecuteNonQuery();
                }
            }
            catch (Exception E)
            {
                Console.WriteLine($"Подключение к базе данных прервано из-за ошибки{E.Message}.\n" +
                   $" Статус подключения {Conn.State}.\n Ошибка произошла в следующем участке кода\n {E.StackTrace}");
            }
        }

        protected void ExecuteTransaction(ObservableCollection<MySqlCommand> commands)
        {
            try
            {
                if (Conn != null && Conn.State == ConnectionState.Open)
                {
                    var Transaction =  Conn.BeginTransaction();
                    try
                    {
                        foreach (var command in commands)
                        {
                            command.Connection = Conn;
                            command.Transaction = Transaction;
                            command.ExecuteNonQuery();
                        }
                         Transaction.Commit();
                    }
                    catch (Exception E)
                    {
                        try
                        {
                             Transaction.Rollback();
                        }
                        catch (MySqlException SQLE)
                        {
                            Console.WriteLine($"Исключение типа {SQLE.GetType()} произошло во время отката транзакции");
                        }
                        Console.WriteLine($"Отмена транзакции была вызвана исключением {E.Message} в следующем учатке кода \n {E.StackTrace}");
                    }
                }
            }
            catch (Exception E)
            {
                Console.WriteLine($"Подключение к базе данных прервано из-за ошибки{E.Message}.\n" +
                   $" Статус подключения {Conn.State}.\n Ошибка произошла в следующем участке кода\n {E.StackTrace}");
            }
        }

        protected abstract ObservableCollection<T> GetData(MySqlCommand command);
        ~MariaDBResolver()
        {
            Console.WriteLine($"Подключение к базе данных. Статус подключения {Conn.State}.");
            Conn.Close();
            Console.WriteLine($"Подключение к базе данных. Статус подключения {Conn.State}.");
        }
    }
}

