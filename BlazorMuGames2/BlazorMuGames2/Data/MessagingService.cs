﻿using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using System;
using System.Linq;
using System.Collections.ObjectModel;
using System.Security.Cryptography.X509Certificates;
using System.Collections.Generic;

namespace BlazorMuGames2.Data
{
    public class DataService : MariaDBResolver<Data>
    {
        private IConfiguration Configuration;
        private Serelizator serelizator = new Serelizator();
        private MySqlCommand SelectData = new MySqlCommand("SELECT * FROM `data`");
        private MySqlCommand InsertData = new MySqlCommand("INSERT INTO `data` (data_obj)VALUES(@Data)");
        public DataService(IConfiguration Configuration) : base(Configuration) => this.Configuration = Configuration;
        protected override ObservableCollection<Data> GetData(MySqlCommand command)
        {
            var DataService = new ObservableCollection<Data>();
            var BufferedData = GetBufferedData(command);
            try
            {
                foreach (var data in BufferedData)
                {
                    var RestoredData = (Data)serelizator.ByteArrayToObject((byte[])data[1]);
                    RestoredData.Id = (uint)data[0];
                    DataService.Add(RestoredData);
                }
            }
            catch (Exception E)
            {
                Console.WriteLine($"Ошибка в получении данных\n {E.Message}\n {E.StackTrace}\n");
            }
            return DataService;
        }
        public ObservableCollection<Data> GetDatas() => GetData(SelectData);
        public void InsertDatas(Data data)
        {
            InsertData.Parameters.AddWithValue("@Data", serelizator.ObjectToByteArray(data));
            RunCommand(InsertData);
            InsertData.Parameters.Clear();
        }
    }

    public class MessagingService : MariaDBResolver<Message>
    {
        private IConfiguration Configuration;
        private Serelizator serelizator = new Serelizator();
        private MySqlCommand SelectMessages = new MySqlCommand("SELECT * FROM message_list");
        private MySqlCommand InsertMessages = new MySqlCommand("INSERT INTO message (id_acc,id_data)VALUES (@Account,@Data)");
        private MySqlCommand SelectMessageLinks = new MySqlCommand("SELECT * FROM messtochat");
        private MySqlCommand InsertMessageLink = new MySqlCommand("INSERT INTO messtochat (id_mess,id_chat) VALUES (@Message,@Chat)");
        public MessagingService(IConfiguration Configuration) : base(Configuration) => this.Configuration = Configuration;
        
        protected override ObservableCollection<Message> GetData(MySqlCommand command)
        {
            var MessagingService = new ObservableCollection<Message>();
            var BufferedData = GetBufferedData(command);
            try
            {
                foreach (var message in BufferedData)
                {
                    var Account = (Account)serelizator.ByteArrayToObject((byte[])message[2]);
                    var DataObj = (Data)serelizator.ByteArrayToObject((byte[])message[4]);
                    Account.Id = (uint)message[1];
                    DataObj.Id = (uint)message[3];
                    MessagingService.Add(new Message { Id = (uint)message[0], Account = Account, Data = DataObj});
                }
            }
            catch (Exception E)
            {
                Console.WriteLine($"Ошибка в получении сообщений\n {E.Message}\n {E.StackTrace}\n");
            }
            return MessagingService;
        }
        public ObservableCollection<Message> GetMessages()=>GetData(SelectMessages);

        public Dictionary<Message, Chat> GetChatMessages()
        {
            var MessageDictionary = new Dictionary<Message, Chat>();
            var Chats = new ChatService(Configuration).GetChats();
            var Messages = GetMessages();
            var BuffferedRecords = GetBufferedData(SelectMessageLinks);
            try
            {
                foreach (var record in BuffferedRecords)
                {
                    var message = from messages in Messages where messages.Id == (uint)record[0] select messages;
                    var chat = from chats in Chats where chats.Id == (uint)record[1] select chats;
                    MessageDictionary.Add(message.First(),chat.First());
                }
            }
            catch (Exception E)
            {
                Console.WriteLine($"Ошибка в получении сообщений\n {E.Message}\n {E.StackTrace}\n");
            }
            return MessageDictionary;
        }
            
        public void InsertMessage(Message message, Chat chat)
        {
            var data = new DataService(Configuration);
            message.Data.CreationDate = DateTime.UtcNow;
            data.InsertDatas(message.Data);
            var InsertedDataId = from datablock in data.GetDatas()
                               where datablock.CreationDate == message.Data.CreationDate && datablock.DataText == message.Data.DataText
                               select datablock;
            InsertMessages.Parameters.AddWithValue("@Account", message.Account.Id);
            InsertMessages.Parameters.AddWithValue("@Data", InsertedDataId.First().Id);
            RunCommand(InsertMessages);
            InsertMessages.Parameters.Clear();
            var InsertedMessage = from messages in GetMessages() where messages.Data.Id == InsertedDataId.First().Id select messages;
            InsertMessageLink.Parameters.AddWithValue("@Message", InsertedMessage.First().Id);
            InsertMessageLink.Parameters.AddWithValue("@Chat", chat.Id);
            RunCommand(InsertMessageLink);
            InsertMessageLink.Parameters.Clear();
        }
    }
}
